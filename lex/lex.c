/*
 * lex.c
 *
 * a hand made DFA lexical analyzer for Oxyl
 * unicode is supported under the assumption that all non-ascii characters are alphabetic
 */

#include "lex/toklist.h"
#include "util/hash.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

const char *toktypenames[] = {
	"End Of File",
	"Integer Literal",
	"Float Literal (standard notation)",
	"Float Literal (exponential notation)",
	"Keyword or Identifier",
	"Literal Character (such as an operator)",
	"New Line",
	"Tab"
};

static int line = 1; // line number

// ordered to minimize comparisons
static inline bool is_alpha(char c) {
	if (c == '_') {
		return true;
	}
	c = c & 0xdf; // remove case
	return (c >= 'A') && (c <= 'Z') || (c > 0x7f) || (c < 0); // char is signed on x86
}

static inline bool is_num(char c) {
	return (c >= '0') && (c <= '9');
}

static inline bool is_anum(char c) {
	return is_alpha(c) || is_num(c);
}

/* this used to only be spaces
 * but it turns out unicode can just dump random ASCII control characters
 * actually that's not true
 */
static inline bool is_ignore(char c) {
//	return (c <= 0x20) && (c > 0);
	return c == ' ';
}

// returns the last index that is still a word -- call after getting an alpha character
static inline int st_word(const char *s) {
	int i;
	for (i = 0; is_anum(s[i]); i++);
	return i;
}

static inline int st_num(const char *s) {
	int i;
	for (i = 0; is_num(s[i]); i++);
	return i;
}

static inline int st_tab(const char *s) {
	int i;
	for (i = 0; i == '\t'; i++);
	return i;
}

static inline int st_comment(const char *s) {
	int i;
	for (i = 0; s[i] != '\n' && s[i] != 0; i++);
	return i;
}

static inline int st_nl(const char *s) {
	int i = 0;
	while (true) {
		if (s[i] != '\n') {
			if (s[i] == '#') {
				i += st_comment(s + i);
			} else if (is_ignore(s[i])) {
			} else {
				return i;
			}
		} else {
			line++;
			i++;
		}
	}
	fprintf(stderr, "Infinity has been completed. Truth is false. We are at the end of time. Now ask yourself, was time good?");
	exit(1);
	fprintf(stderr, "Something is very, very wrong. What a strange fate the fabric of reality is in, isn't it");
	return i;
}

static inline int st_str(const char *s) {
	int i;
	for (i = 0; i != '"'; i++);
	return i;
}

static inline char *substr(const char *s, int i, int n) {
	char *r = malloc(n + 1);
	strncpy(r, s + i, n);
	return r;
}

static inline struct toklist *new_toklist(void) {
	struct toklist *r = malloc(sizeof(struct toklist));
	if (r == NULL) {
		fprintf(stderr, "Cannot allocate toklist.\n");
		exit(2);
	}
	r->type = TOK_EOF;
	return r;
}

struct toklist *lex(const char *s) {
	line = 1;
	int i;
	struct toklist *tl = new_toklist();
	struct toklist *t = tl;
	t->next = NULL;
	t->v = NULL; // we'll use this later to return null on a null file
	int len = strlen(s);
	for (i = 0; i < len;) {
		if (t->next) {
			t = t->next;
		}
		t->i = i;
		if (is_ignore(s[i])) {
			i++; // this was a normal for loop, but I think that caused problems
			continue;
		}
		if (is_alpha(s[i])) {
			int il = i;
			int ir = i += st_word(s + i);
			t->type = TOK_WORD;
			t->v = substr(s, il, ir - il);
			t->hash = hash(t->v);
			t->line = line;
			t->next = new_toklist();
			continue;
		}
		if (is_num(s[i])) {
			int il = i;
			int ir = i += st_num(s + i);
			t->type = TOK_INT;
			t->v = substr(s, il, ir - il); // this is only needed for testing
			t->iv = atoi(t->v);
			t->line = line;
			t->next = new_toklist();
			continue;
		}
		if (s[i] == '\n' || s[i] == '#') {
			i += st_nl(s + i);
			t->type = TOK_NL;
			t->line = line;
			t->next = new_toklist();
			continue;
		}
		if (s[i] == '\t') {
//			int id = st_tab(s + i); // not how the parser works
			t->type = TOK_TAB;
//			t->n = id;
//			i += id;
			i++;
			t->line = line;
			t->next = new_toklist();
			continue;
		}
		// fall back by calling it a literal
		t->type = TOK_LITC;
		t->v = substr(s, i, 1);
		t->line = line;
		t->next = new_toklist();
		i++;
	}
	if (t->v == NULL && t->next == NULL) {
		return NULL;
	}
	t->next = NULL;
	return tl;
}

#ifndef DONT_TEST

#include "mtest.h"

int test_lex_int(void) {
	mthead;
	mttrue(is_num('0'));
	struct toklist *fred = lex("0");
	mteqs(fred->v, "0");
	mteqi(fred->type, TOK_INT);
	mteqi(fred->iv, 0);
	mtnull(fred->next);
	fred = lex("348");
	mteqi(fred->type, TOK_INT);
	mteqi(fred->iv, 348);
	mtnull(fred->next);
	mttail;
}

int test_lex_word(void) {
	mthead;
	mttrue(is_alpha('q'));
	mttrue(is_alpha('a'));
	mttrue(is_alpha('z'));
	mttrue(is_alpha('_'));
	struct toklist *fred = lex("a_w2ord");
	mteqi(fred->type, TOK_WORD);
	mteqs(fred->v, "a_w2ord");
	mteqi(fred->hash, hash("a_w2ord"));
	mtnull(fred->next);
	mttail;
}

int test_lex_unicode(void) {
	mthead;
	mttrue(is_alpha(80));
	mtfalse(is_ignore('\343'));
	mttrue(is_alpha('\343'));
	struct toklist *fred = lex("いち一");
	mteqi(fred->type, TOK_WORD);
	mteqs(fred->v, "いち一");
	mteqi(fred->hash, hash("いち一"));
	mtnull(fred->next);
	mttail;
}

int test_lex_comment(void) {
	mthead;
	struct toklist *fred = lex("# comment 69\n");
	mteqi(fred->type, TOK_NL);
	mtnull(fred->next);
	fred = lex("# comment at end of file");
	mteqi(fred->type, TOK_NL);
	mtnull(fred->next);
	mttail;
}

int test_lex_litc(void) {
	mthead;
	struct toklist *fred = lex("+");
	mteqi(fred->type, TOK_LITC);
	mteqs(fred->v, "+");
	mttail;
}

int test_lex(void) {
	mthead;
	struct toklist *tl = lex(" \n\tfoo#\n3");
	mtnnull(tl);
	struct toklist *fred = tl;
	mteqi(fred->i, 1);
	mteqi(fred->type, TOK_NL);
	fred = fred->next;
	mtnnull(fred);
	mteqi(fred->type, TOK_TAB);
	mteqi(fred->i, 2);
	fred = fred->next;
	mtnnull(fred);
	mteqi(fred->type, TOK_WORD);
	mteqs(fred->v, "foo");
	mteqi(fred->i, 3);
	fred = fred->next;
	mtnnull(fred);
	mteqi(fred->type, TOK_NL);
	mteqi(fred->i, 6);
	fred = fred->next;
	mtnnull(fred);
	mteqi(fred->type, TOK_INT);
	mteqi(fred->iv, 3);
	mteqi(fred->i, 8);
	mtnull(fred->next);
	mttail;
}

int test_lineno_static(void) {
	mthead;
	struct toklist *tl = lex("shn\nabrt\n\nfoi");
	mtnnull(tl);
	mtnnull(tl->next);
	mteqi(tl->line, 1);
	struct toklist *fred = tl->next->next;
	mtnnull(fred);
	mtnnull(fred->next);
	mteqi(fred->line, 2);
	fred = fred->next->next;
	mtnnull(fred);
	mteqi(fred->line, 4);
	mttail;
}

#define FUZZ_SIZE 512

int test_lineno_fuzz(void) {
	mthead;
	char *s = malloc(FUZZ_SIZE + 1);
	s[0] = '0';
	s[1] = 'a';
	int line = 0;
	for (int i = 2; i < FUZZ_SIZE; i++) {
		s[i] = rand() & 0xff;
		if (s[i] == 0) {
			s[i] = 'q';
		} else if (s[i] == '\n' || (s[i] & (rand() & 0x3)) == 0x3) {
			s[i] = '\n';
			line++;
			if (i < 10) {
				s[i + 1] = '0' + line;
				s[i + 2] = 'b';
				i += 3;
			} else if (i < 100) {
				s[i + 1] = '0' + line / 10;
				s[i + 2] = '0' + line % 10;
				s[i + 3] = 'c';
				i += 4;
			} else {
				s[i] = 0;
				break;
			}
		}
	}
	s[FUZZ_SIZE] = 0;
	struct toklist *tl = lex(s);
	mtnnull(tl);
	struct toklist *fred = tl;
	for (int i = 0; fred; i++) {
		mteqi(fred->type, TOK_INT);
		mteqi(i, fred->iv);
		for (; (fred != NULL) && fred->type != TOK_NL; fred = fred->next);
		fred = fred->next;
	}
	free(s);
	mttail;
}

#endif
