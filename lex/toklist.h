/*
 * toklist.h
 *
 * defines a datastructure to hold a list of tokens
 */

#ifndef LEX_TOKLIST_H
#define LEX_TOKLIST_H

enum toktype {
	TOK_EOF = 0, // 0
	TOK_INT = 1, // 1
	TOK_FSTD = 2, // 2 normal 3.14 notation
	TOK_FEXP = 3, // 3 6.02e21 notation
	TOK_WORD = 4, // 4 any normal kind of word
	TOK_LITC = 5, // 5 litaral character (e.g. an operator)
	TOK_NL = 6, // 6
	TOK_TAB = 7 // 7
};

extern const char* toktypenames[]; // names of token types

struct toklist {
	char *v;
	enum toktype type;
	union { // depends on type
		int iv; // interger value
		double fv; // floating point value
		int hash; // hash of the string
		int n; // number of things
	};
	int line;
	int i; // the index in the string (used for debugging)
	struct toklist *next;
};

struct toklist *lex(const char *string);

#endif
