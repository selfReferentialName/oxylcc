/**
 * @file mrun.h
 *
 * @brief minimal runtime assertations
 *
 * this is intended to help make sure that programs won't accidentally crash
 */

#ifndef MRUN_H
#define MRUN_H

#include <stdlib.h>
#include <errno.h>

#define mrnnull(x) do { \
	if (x == NULL) { \
		fprintf(stderr, "Unnexpected null (%s, file %s, func %s, line %d)\n", #x, __FILE__, __func__, __LINE__); \
		fprintf(stderr, "Errno reads: %s.\n", strerror(errno)); \
		exit(3); \
	}} while (0)

#define mrposi(x) do { \
	int mrposi_x; \
	if ((mrposi_x = x) <= 0) { \
		fprintf(stderr, "Unnexpected non-positive integer %d (%s, file %s, func %s, line %d)\n", mrposi_x, #x, __FILE__, __func__, __LINE__); \
		exit(3); \
	}} while (0)

#endif
