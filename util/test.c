/**
 * @file test.c
 *
 * @brief unit tests for the utils header files
 */

#include "mtest.h"
#include "util/muid.h"
#include "util/hash.h"
#include <stdlib.h>
#include <time.h>
#include <stdio.h>

#define FUZZ_SIZE 8

int test_muid_fuzz(void) {
	mthead;
	char *muids[FUZZ_SIZE];
	for (int i = 0; i < FUZZ_SIZE; i++) {
		muids[i] = muid();
		for (int j = 0; j < i; j++) {
			mtdifs(muids[i], muids[j]);
		}
	}
	mttail;
}
