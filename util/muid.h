/**
 * @file util/muid.h
 *
 * @brief Module Unique IDentifier
 */

#ifndef UTIL_MUID_H
#define UTIL_MUID_H

/**
 * @brief create a muid
 * @return the muid string
 */
char *muid (void);

#endif
