/**
 * @file util/muid.c
 *
 * @brief Module Unique IDentifier
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/**
 * @brief create a muid
 * @return the muid string
 */
char *muid (void) {
	static i;
	char *acc = calloc(1, 11);
	snprintf(acc, 11, ".m%d", i++);
	return acc;
}
