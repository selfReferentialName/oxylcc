/**
 * @file expression.h
 *
 * @brief all kinds of expressions
 */

#ifndef AST_EXPRESSION_H
#define AST_EXPRESSION_H

#include "ast/idht.h"
#include <stdbool.h>

/**
 * @brief a function call
 */
struct expr_call {
	struct idhte func; /**< the idhte of the function */
	int argc; /**< number of arguments */
	struct expr *argv; /**< the arguments themselves */
};

/**
 * @brief an identifer
 * turns an identifier of a variable into an expression
 */
struct expr_id {
	struct idhte *id; /**< the id */
};

/**
 * @brief a literal
 * a literal of any type
 */
struct expr_lit {
	enum {
		LIT_INT, /**< interger literal */
		LIT_FLOAT /**< floating point (double) literal */
	} type; /**< what value the literal is */
	union {
		int i; /**< the value as an interger */
		double f; /**< the value as a float */
	};
};

/**
 * @brief an operation
 * an operation of two different expressions
 */
struct expr_op {
	struct expr *a; /**< the left of the operation */
	struct expr *b; /**< the right of the operation */
	char op; /**< the character denoting the operation -- ASCII only. if (op & 0x80), append an '=' */
};

/**
 * @brief all types of expression
 * this is all types with a distinct AST struct
 */
enum expr_type {
	EXPR_CALL, /**< expr_call */
	EXPR_OP, /**< expr_op */
	EXPR_ID, /**< expr_id */
	EXPR_LIT, /**< expr_lit */
	EXPR_VOID /**< no fields of the expression are valid */
};

/**
 * @brief a generic expressoin
 * a way to hold any type of expression
 */
struct expr {
	union {
		/**
		 * @brief the expression is a call
		 * because calls can have any number of arguments, this is a pointer
		 * it must be malloced
		 */
		struct expr_call* call;
		struct expr_op op; /**< the expression is an operation */
		struct expr_id id; /**< the expression is an id */
		struct expr_lit lit; /**< the expression is a literal */
	};
	enum expr_type type; /**< the type of the expression */
	bool ivc; /**< true iff the expression is to be IVCed */
	struct xltype *xtyp; /**< the result type -- propogated up in codegen, not valid in parsing */
};

#endif
