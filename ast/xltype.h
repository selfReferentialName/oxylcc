/*
 * @file xltype.h
 *
 * ast for Oxyl native types
 * since this is used only for conversions and knowing how to parse, not all info is included
 *
 * 
 */

#ifndef AST_XLTYPE_H
#define AST_XLTYPE_H

#include <stdbool.h>

/**
 * @brief this type needs a contained type
 * if a type with this bit set has cont == NULL, there is a syntax error
 * else, there should be no contained type
 */
#define EXPECT_CONT (1 << 0)
/**
 * @brief this type needs an x dimension
 * this bit is set iff x is valid
 * this bit is set in, e.g. vec
 */
#define EXPECT_X (1 << 1)
/**
 * @brief this type needs a y dimension
 * this bit is set iff y is valid
 * this bit is set in, e.g. mat
 */
#define EXPECT_Y (1 << 2)

/**
 * @brief please ignore
 * this is used internally
 * it is opaque here to aviod a circular dependency
 */
struct idhte;

/**
 * @brief a type in a form nice to work with in AST
 */
struct xltype {
	char *t; /**< the name of the type */
	struct xltype *cont; /**< contained type, e.g. array<cont> */
	int x; /**< size in the x dimension -- may be the only dimension */
	int y; /**< size in the y dimension */
	struct idhte **members; /**< an idht of all member variables iff this is a class */
	/**
	 * @brief an idht of all methods iff this is a class 
	 * constructors are all named .construct
	 * operator overloads all start with .op.
	 */
	struct idhte **methods;
	void *lltype; /**< the type as an LLVMTypeRef */
	/**
	 * @brief this is where to put in all the EXPECT_ flags
	 * the EXPECT_ flags show what should be between the <>s
	 * if this is 0, there should be no <>s
	 * this is parsed in the order of EXPECT_CONT, EXPECT_X, EXPECT_Y
	 * between each expected thing, there is a comma
	 */
	int flags;
	bool reg; /**< this is true for and only for ints, floats, and vectors thereof -- it means that it should be passed by value */
	bool fp; /**< this is true for and only for floats and vectors thereof. (reg && !fp) means it is an int */
};

#endif
