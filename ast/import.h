/**
 * @file ast/import.h
 * @brief import statement AST data
 *
 * import statements are given as a string representing the file name
 * this isn't that different from how it's put in, but requires copious use of strcat
 */

#ifndef AST_IMPORT_H
#define AST_IMPORT_H

#include <string.h>
#include <stdlib.h>

typedef char *import;

/**
 * @brief concate two import statements
 *
 * adds a string to the base import statement with a / between them
 * @param statement the base statement
 * @param next the next word of the statement as a character
 * @ruturn the concated import statement
 */
inline import imp_cat(import statement, char *next) {
	if (next) {
		import acc = malloc(strlen(statement) + strlen(next) + 2);
		strcpy(acc, statement);
		strcat(acc, "/");
		strcat(acc, next);
		return acc;
	} else {
		return NULL;
	}
}

#endif
