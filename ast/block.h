/*
 * block.h
 *
 * handles all kinds of blocks
 * function blocks, conditional blocks, etc
 */

#ifndef AST_BLOCK_H
#define AST_BLOCK_H

#include "ast/idht.h"
#include "ast/expression.h"

struct block_func {
	struct idhte *id;
	int argc;
	struct idhte *argv;
	struct block *code;
	idht locals;
};

// generic block
// it could be a line
// the important thing is that it is all under the same block in generated code
struct block {
	enum {
		BLOCK_EXPR, // this one is a line
		BLOCK_RET, // identical to BLOCK_EXPR, but the value is returned and exp can be of type EXPR_VOID (this is the only place that type is valid)
		BLOCK_FUNC,
		BLOCK_FILE,
		BLOCK_ERROR // temporarily used before the actual type has been parsed, invalid in codegen
	} type;
	union {
		struct block_func func;
		struct expr exp;
	};
	struct block *next;
};

#endif
