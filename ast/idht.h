/**
 * @file ast/idht.h
 * @brief IDentifier Hash Table
 */

#ifndef AST_IDHT_H
#define AST_IDHT_H

#define IDHT_SIZE 4096
#define IDHT_MASK(x) (x & 0xfff)

#include "ast/xltype.h"
#include <stdbool.h>

/**
 * @brief An entry in an ID Hash Table
 *
 * This contains information about stuff like identifiers
 * It isn't strictly used for IDHTs
 * But that is the primary usage
 */
struct idhte {
	union {
		char *key; /**< key for stuff in a hash table -- it should be the visible name */
		void *dat; /**< data representation pointer for things not in a hash table */
	};
	struct idhte *next; /**< INTERNAL USE ONLY -- the next entry with a hash collision */
	void *lltype; /**< the type as an llvm object */
	union {
		void *hlval; /**< the temporary high level value of the entry to be replaced by llval during codegen */
		void *llval; /**< the LLVM representation of the entry */
		void *parent; /**< the parent function of the parameter */
	};
	struct xltype *hltype; /**< the type as seen in oxyl */
	bool isid; /**< true iff key is valid, false iff dat is valid -- all entries of an idht have this as true, but not all idhte objects with this set are in an idht */
	bool isparam; /**< true iff the value is actually a function parameter -- true: parent is valid, false: llval is valid */
	int index; /**< the index for use in structures and parameters -- pass this along to extractelement and insertelement */
};

/**
 * @brief data to enumerate an idht
 * 
 * the only important thing is the dat element
 */
struct idht_iter {
	struct idhte *dat; /**< the entry */
	int i; /**< internally used to track position */
};

/**
 * @brief An actual ID Hash Table
 *
 * Consider this as an opaque datatype
 */
typedef struct idhte **idht;

/**
 * @brief find an entry in an idht
 *
 * if it isn't found, NULL is returned, making it also function as an is_in_idht function
 * NULL is also returned if the table is NULL
 *
 * @param table the table to search in
 * @param key the key to search for
 * @return the entry that matches the key
 */
struct idhte *idht_index(const idht table, const char* key);

/**
 * @brief enter something into an idht
 *
 * the key is taken from the entry and is not a separate parameter
 *
 * @param table the table to put it into
 * @param entry the entry to put into the table
 * @warning various assertations are done and if any fail, the program will die
 */
void idht_enter(idht table, struct idhte *entry);

/**
 * @brief construct a new idht
 *
 * does what it says on the tin
 */
idht idht_new(void);

/**
 * @brief construct a new idhte
 *
 * puts in some sane defaults
 * @param key the key to use
 * @param type the type of the entry
 */
struct idhte *idhte_new(const char *key, const struct xltype *type);

/**
 * @brief construct a new idhte for an lltype, creating the xltype to go with it
 *
 * @param key the key to use
 * @param type the lltype to use
 */
struct idhte *idhte_llnew(const char *key, void *type);

/**
 * @brief construct a new idhte
 *
 * this version is designed for use in codegen
 * @param key the key to use
 * @param val the llvalue to use
 */
struct idhte *idhte_valued(char *key, void *val);

/**
 * @brief enumerate an idht
 *
 * here is the documentation for an idht_iter for convienience
 * an idht_iter is data to enumerate an idht
 * the only important thing is the dat element
 *
 * @param table the table to iterate
 * @param last the previous result of iteration or something with dat set to NULL
 * @return the next result of iteration or null
 */
struct idht_iter *idht_next(const idht table, struct idht_iter *last);

#define IDHT_FOREACH(var, table) \
	struct idht_iter *idht_foreach_i; \
	idht_foreach_i->dat = 0; \
	var = (idht_foreach_i = idht_next(table, idht_foreach_i))->dat; \
	for (;(idht_foreach_i = idht_next(table, idht_foreach_i)); var = idht_foreach_i->dat)

#endif
