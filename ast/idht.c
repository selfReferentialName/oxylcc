/*
 * idht.c
 *
 * work with IDHTs
 */

#include "ast/idht.h"
#include "util/hash.h"
#include "ast/xltype.h"
#include <string.h>
#include <stdlib.h>
#include <assert.h>

idht idht_new(void) {
	return calloc(IDHT_SIZE, sizeof(struct idhte*));
}

void idht_enter(idht table, struct idhte* entry) {
	assert(entry->isid);
	assert(table != NULL);
	int h = IDHT_MASK(hash(entry->key));
	if (table[h]) { // hash collision
		entry->next = table[h];
	} else {
		entry->next = NULL;
	}
	table[h] = entry;
}

struct idhte* idht_index(const idht table, const char *key) {
	if (table == NULL) {
		return NULL;
	}
	struct idhte* c = table[IDHT_MASK(hash(key))];
	if (c == NULL) {
		return NULL;
	}
	while (strcmp(key, c->key)) { // while not equal
		c = c->next;
		if (c == NULL) {
			return NULL;
		}
	}
	return c;
}

struct idht_iter *idht_next(const idht table, struct idht_iter *last) {
	if (last->dat == NULL) {
		last->i = 0;
		last->dat = table[0];
	} else {
		last->dat = last->dat->next;
	}
	while (last->dat == NULL) {
		last->dat = table[++last->i];
		if (last->i > IDHT_SIZE) {
			return NULL;
		}
	}
	return last;
}

struct idhte *idhte_new(const char *key, const struct xltype *type) {
	struct idhte *acc = malloc(sizeof(struct idhte));
	acc->key = key;
	acc->next = NULL;
	acc->lltype = type->lltype;
	acc->llval = NULL;
	acc->hltype = type;
	acc->isid = true;
	acc->isparam = false;
	return acc;
}

struct idhte *idhte_llnew(const char *key, void *type) {
	struct xltype *acc = malloc(sizeof(struct xltype));
	acc->t = key;
	acc->cont = NULL;
	acc->flags = 0;
	acc->reg = false;
	acc->fp = false;
	acc->members = NULL;
	acc->methods = NULL;
	acc->lltype = type;
	return idhte_new(key, acc);
}

struct idhte *idhte_value(const char *key, const void *val) {
	struct idhte *acc = malloc(sizeof(struct idhte));
	acc->key = key;
	acc->next = NULL;
	acc->lltype = NULL;
	acc->llval = val;
	acc->hltype = NULL;
	acc->isid = true;
	acc->isparam = false;
	return acc;
}
