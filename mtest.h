/**
 * @file mtest.h
 *
 * @brief extremely simple unit testing framework
 *
 * this header is under the following license:
 *
 *         DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2019 J Rain De Jager <rain.osdev@gmail.com>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *
 * make sure to #include <stdio.h> at the end
 */

#ifndef mthead


/**
 * @brief put this at the top of any testing function
 */
#define mthead int mterror = 0

/**
 * @brief assert that something is true
 *
 * use to make an assertation statement
 */
#define mttrue(x) do { if (!x) { \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %s was not true (file %s, line %d).\n", \
				__func__, #x, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief assert that two integers things are equal
 */
#define mteqi(x, y) do { if (x != y) { \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %d was inequal to %d (file %s, line %d).\n", \
				__func__, x, y, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief assert that two strings are inequal
 *
 * <string.h> must be included
 */
#define mtdifs(x, y) do { if (!strcmp(x, y)) { \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %s was equal to %s (file %s, line %d).\n", \
				__func__, x, y, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief place this in code that shouldn't be reached
 */
#define mtunreachable do { \
		mterror++; \
		printf("%s failed:\nmtunreachable has been executed (file %s, line %d).\n", \
				__func__, __FILE__, __LINE__); \
	} while (0)

/**
 * @brief assert something is false
 */
#define mtfalse(x) do { if (x) { \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %s was true (file %s, line %d).\n", \
				__func__, #x, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief assert something is NULL
 */
#define mtnull(x) do { if (x != NULL) { \
		void *mt__ptr = x; \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %s == %p was non-null (file %s, line %d).\n", \
				__func__, #x, mt__ptr, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief assert two strings are equal
 */
#define mteqs(x, y) do { if (strcmp(x, y)) { \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %s was inequal to %s (file %s, line %d).\n", \
				__func__, x, y, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief assert that something isn't NULL
 */
#define mtnnull(x) do { if (x == NULL) { \
		mterror++; \
		printf("%s failed:\nmtest assertation failed: %s was null (file %s, line %d).\n", \
				__func__, #x, __FILE__, __LINE__); \
	}} while (0)

/**
 * @brief checkpoint printing thingy
 * @param msg the message to print
 */
#define mtcheck(msg) printf("Checkpoint in %s: %s (file %s, line %d).\n", __func__, msg, __FILE__, __LINE__)

/**
 * @brief put this at the end of any testing function
 */
#define mttail return mterror

#ifndef mttail
#error "preprocessor failure"
#endif

#endif
