(* default bitcode output *)
let dbitcode file =
	Filename.remove_extension file ^ ".bc";;

let main file bitcode =
	let bco = if bitcode = "" then dbitcode file else bitcode in
    print_string "Given file ";
    print_string file;
    print_string " to output to ";
    print_string bco;
    print_string "\n";;

open Cmdliner

let obc =
	let doc = "The file to output bitcode (instead of just changing the exension to bc).\n" in
	Arg.(value & opt string "" & info ["C"; "bitcode"] ~docv:"FILE" ~doc);;

let ofile =
	let doc = "The file to compile.\n" in
	Arg.(value & pos 0 string "in.bc" & info [] ~docv:"FILE" ~doc);;

let info =
	let doc = "Compile an Oxyl file.\n" in
	let man = [
		`S Manpage.s_bugs;
		`P "Put any bug reports in the issue tracker at https://gitlab.com/selfReferentialName/oxylc" ] in
	let exits = Term.default_exits in
	Term.info "oxylc" ~version:"0.1.0" ~doc ~exits ~man;;

let oxylc_t = Term.(const main $ ofile $ obc)

let () = Term.exit @@ Term.eval (oxylc_t, info)
