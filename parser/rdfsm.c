/**
 * @file parser/rdfsm.c
 *
 * @brief the parser, but this time using a DFSM derived system
 *
 * this version uses the RDFSM algorithm
 * Recursive
 * Deterministic
 * Finite
 * State
 * Machine
 * essentially, there are a bunch of DFSMs that can call other DFSMs and stuff
 * each DFSM that stays on one state is in one function
 * any function call to another parsing function stores its own logical stack
 * @note the prefix for an FSM is m_
 */

#include "lex/toklist.h"
#include "ast/expression.h"
#include "ast/block.h"
#include "ast/import.h"
#include "ast/xltype.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <llvm-c/Core.h>
#include "mrun.h"
#include <limits.h>

extern bool verbose;

#ifdef __clang__
#define always_inline __attribute__((always_inline))
#else
#define always_inline inline
#endif

struct pinfo {
	struct {
		int rbp;
		int tl;
	};
	void *left;
	idht types;
	idht locals;
	idht globals;
	idht funcs;
	bool inexp; /**< true iff the function is called by m_exp */
};

/**
 * @brief sanity check that info is good for file scope
 */
static always_inline void c_file(const struct pinfo info) {
	mrnnull(info.types);
	mrnnull(info.globals);
}

/**
 * @brief sanity check that info is good for function scope
 */
static always_inline void c_func(const struct pinfo info) {
	c_file(info);
	mrnnull(info.locals);
	mrposi(info.tl);
}

/**
 * @brief sanity check that info is good for expression continuation scope
 *
static always_inline void c_expr(const struct pinfo info) {
	c_func(info);
//	mrnnull(left);
} */

static struct pinfo ignore; // a meaningless pinfo struct containing undefined data not to be used

/**
 * @brief report a syntax error
 *
 * @param context the context of the error
 * @param msg the custom message to send (should be empty or end in a newline)
 * @param tok the token
 * @param failp the pointer to show error
 */
static always_inline void error(const char *context, const char *msg, const struct toklist *tok, bool *failp) {
	fprintf(stderr, "Syntax error on line %d at token %s (type %s) in context %s.\n%s", tok->line, tok->v, toktypenames[tok->type], context, msg);
	*failp = true;
}

#define tl (*tlp)

/**
 * @brief the parsing function for an import statement
 * @param tlp a pointer to the token list (the tl macro expands to a dereference of tlp
 * tlp is a pointer so that child FSMs will advance the actual input
 * since tl is a macro, tlp will get updated
 * @param info the information to be passed through each FSM
 * @param failp a pointer to a bool to show failure (used through the error function)
 * @note this only runs s_imp
 */
static always_inline void m_imp(struct toklist **tlp, struct pinfo info, bool *failp) { 
	mrnnull(tl);
	c_file(info);
	printf("TODO: import stuff");
}

int rbp[256]; /**< indexed by character, determines the tendency to push a new m_exp */
int lbp[256]; /**< indexed by character, determines the tendency to act as an input to the operation to the right (overrides equal rbp) */

/**
 * @brief initialize the rbp and lbp arrays
 * run once on startup
 */
static always_inline void init_bp(void) {
	rbp['='] = lbp['='] = 0;
	rbp['&'] = lbp['&'] = rbp['|'] = lbp['|'] = rbp['^'] = lbp['^'] = 1;
	rbp['<'] = lbp['<'] = rbp['>'] = lbp['>'] = rbp['<' | 0x80] = lbp['<' | 0x80] = rbp['>' | 0x80] = lbp['>' | 0x80] = rbp['=' | 0x80] = lbp['=' | 0x80] = 2;
	rbp['+'] = lbp['+'] = rbp['-'] = lbp['-'] = 3;
	rbp['*'] = lbp['*'] = rbp['/'] = lbp['/'] = rbp['%'] = lbp['%'] = 4;
	rbp['.'] = lbp['.'] = rbp[':'] = lbp[':'] = 5;
}

static always_inline struct expr *m_exp(struct toklist **tlp, struct pinfo info, bool *failp, struct xltype *typ);

/**
 * @brief the parsing function for an expression terminator (everything but an operator)
 * @param tlp a pointer to the token list (the tl macro expands to a dereference of tlp
 * tlp is a pointer so that child FSMs will advance the actual input
 * since tl is a macro, tlp will get updated
 * @param info the information to be passed through each FSM
 * @param failp a pointer to a bool to show failure (used through the error function)
 * @param typ the type of the expression (for top-down propogation) or NULL if unknown
 * @note this only runs the s_ex class
 */
static always_inline struct expr *m_ex_term(struct toklist **tlp, struct pinfo info, bool *failp, struct xltype *typ) {
	mrnnull(tl);
	struct expr *acc = malloc(sizeof(struct expr));
	mrnnull(acc);
	acc->type = EXPR_VOID; // make codegen sanity checks work
	acc->xtyp = typ;
	acc->ivc = false;
	struct idhte *f;
	switch (tl->type) {
		case TOK_LITC: // this call was invalid
			free(acc);
			return NULL;
		case TOK_INT: // s_ex_int
			acc->xtyp->reg = true;
			acc->xtyp->fp = false;
			acc->type = EXPR_LIT;
			acc->lit.type = LIT_INT;
			acc->lit.i = tl->iv;
			return acc;
		case TOK_FSTD:
		case TOK_FEXP: // s_ex_float
			acc->xtyp->reg = true;
			acc->xtyp->fp = true;
			acc->type = EXPR_LIT;
			acc->lit.type = LIT_FLOAT;
			acc->lit.f = tl->fv;
			return acc;
		case TOK_WORD:
			if ((f = idht_index(info.funcs, tl->v))) { // s_ex_func
				acc->type = EXPR_CALL;
				acc->call->func = *f;
				acc->call->argc = ((struct block_func*) f->hlval)->argc;
				acc->call->argv = malloc(sizeof(struct expr) * acc->call->argc);
				for (int i = 0; i < acc->call->argc; i++) {
					memcpy(&acc->call->argv[i], m_exp(tlp, info, failp, ((struct block_func*) f->hlval)->argv[i].hltype), sizeof(struct expr));
				}
				return acc;
			} else { // s_ex_id
				struct idhte *id = idht_index(info.locals, tl->v);
				if (id == NULL) {
					id = idht_index(info.globals, tl->v);
				}
				if (id == NULL) {
					error("expression terminal", "No prior usage of the given identifier.\n", tl, failp);
					return acc;
				}
				acc->type = EXPR_ID;
				acc->id.id = id;
				return acc;
			}
		default:
			error("expression terminal", "Invalid token type.\n", tl, failp);
			return acc; // this is invalid anyways, it shouldn't be NULL, since that implies that it's an operator
	}
}

/**
 * @brief the parsing function for an expression
 * @param tlp a pointer to the token list (the tl macro expands to a dereference of tlp
 * tlp is a pointer so that child FSMs will advance the actual input
 * since tl is a macro, tlp will get updated
 * @param info the information to be passed through each FSM
 * @param failp a pointer to a bool to show failure (used through the error function)
 * @param typ the type of the expression (for top-down propogation) or NULL if unknown
 * @note this only runs s_exp and s_exp_done
 */
static always_inline struct expr *m_exp(struct toklist **tlp, struct pinfo info, bool *failp, struct xltype *typ) {
	bool inexp = info.inexp; // save a copy of this since we'll overrite it to make code look nicer
	info.inexp = true;
	int bp = INT_MAX;
	struct expr *acc;
	struct expr *left = m_ex_term(tlp, info, failp, typ);
	while (tl->next && tl->next->type == TOK_LITC) {
		acc = malloc(sizeof(struct expr));
		acc->type = EXPR_OP;
		acc->xtyp = typ;
		if (tl->type == TOK_LITC) {
			switch (tl->v[0]) {
//				case '[': // IVC is a big TODO
//					acc->ivc = true;
//					break;
				default:
					error("expression beginning", "Invalid unary operator.\n", tl, failp);
			}
			tl = tl->next;
		} else {
			acc->ivc = false;
		}
		acc->op.a = left;
		if (left == NULL) {
			error("expression beginning", "Unary operators are not yet implemented.\n", tl, failp);
			return NULL;
		}
		acc->op.op = tl->next->v[0];
		if (bp != INT_MAX && lbp[acc->op.op] < bp) {
			break;
		}
		if (tl->next->next->type == TOK_LITC) {
			if (tl->next->next->v[0] != '=') {
				error("binary operation", "Unary operators are not yet implemented.\n", tl, failp);
			}
			tl = tl->next;
			acc->op.op |= 0x80;
		}
		tl = tl->next->next;
		if (rbp[acc->op.op] > bp) {
			acc->op.b = m_exp(tlp, info, failp, typ);
		} else {
			acc->op.b = m_ex_term(tlp, info, failp, typ);
		}
		left = acc;
		bp = lbp[acc->op.op];
		tl = tl->next;
		if (tl == NULL || tl->type == TOK_NL || (tl->type == TOK_LITC && tl->v[0] == '[')) {
			break;
		}
	}
	if (acc && left != acc) { // speculative acc to be destroyed
//		free(acc);
	}
	return left;
}

static always_inline struct block *s_defn (struct toklist **tlp, struct pinfo info, bool *failp, struct xltype *typ, struct idhte *id) {
	if (tl->v[0] != '=') {
		error("definition", "Expected assignment operator.\n", tl, failp);
		return NULL;
	}
	tl = tl->next;
	if (tl == NULL) {
		error("definiton", "Unnexpected EOF.\n", tl, failp);
		return NULL;
	}
	if (info.locals) {
		struct expr *defn = malloc(sizeof(struct expr));
		defn->type = EXPR_OP;
		defn->ivc = false;
		defn->xtyp = typ;
//		defn->op = malloc(sizeof(struct expr_op)); // I'm dumb
		defn->op.op = '=';
		defn->op.b = m_exp(tlp, info, failp, typ);
		defn->op.a = malloc(sizeof(struct expr));
		defn->op.a->type = EXPR_ID;
		defn->op.a->ivc = false;
		defn->op.a->xtyp = typ;
		defn->op.a->id.id = id;
		struct block *defnout = malloc(sizeof(struct block));
		defnout->type = BLOCK_EXPR;
		defnout->exp = *defn;
		defnout->next = NULL;
		tl = tl->next;
		return defnout;
	} else {
		return NULL;
	}
}

/**
 * @brief the parsing function for a definition
 * @param tlp a pointer to the token list (the tl macro expands to a dereference of tlp)
 * tlp is a pointer so that child FSMs will advance the actual input
 * since tl is a macro, tlp will get updated
 * @param info the information to be passed through each FSM
 * @param failp a pointer to a bool to show failure (used through the error function)
 * @param typ the type of the declaration
 * @note this only runs s_decl, s_defn, s_fdecl, s_fdecl_end, s_fdefn
 */
static always_inline struct block *m_defn(struct toklist **tlp, struct pinfo info, bool *failp, struct xltype *typ) {
	mrnnull(tl);
	tl = tl->next;
	mrnnull(tl);
	c_file(info);
//s_decl:
	if (tl->type != TOK_WORD) {
		error("declaration", "Expected identifier.\n", tl, failp);
		return NULL;
	} else if (idht_index(info.types, tl->v)) {
		error("declaration", "Unnexpected type name.\n", tl, failp);
		return NULL;
	} else if (idht_index(info.locals, tl->v)) {
		error("declaration", "Double defenition.\n", tl, failp);
		return NULL;
	}
	struct idhte *id = malloc(sizeof(struct idhte));
	id->key = tl->v;
	id->lltype = NULL;
	id->hltype = typ;
	id->isid = true;
	id->isparam = false;
	tl = tl->next;
	if (info.locals) {
		idht_enter(info.locals, id);
		if (tl->type != TOK_LITC) {
			error("definition", "Expected operator. Possible nested function definition.\n", tl, failp);
			return NULL;
		}
		return s_defn(tlp, info, failp, typ, id);
	} else if (tl == NULL) {
		error("declaration", "Unnexpected EOF.\n", tl, failp);
		return NULL;
	} else if (tl->type == TOK_WORD) {
//		goto s_fdecl; // fall through
	} else if (tl->type == TOK_LITC) {
		if (info.locals) {
			idht_enter(info.locals, id);
		} else {
			idht_enter(info.globals, id);
		}
		return s_defn(tlp, info, failp, typ, id);
	} else {
		error("declaration", "Invalid token type.\n", tl, failp);
		return NULL;
	}
//s_fdecl:
	struct block_func *func = malloc(sizeof(struct block_func));
	idht_enter(info.funcs, id);
	id->hlval = func;
	func->id = id;
	func->argc = 0;
	func->argv = NULL;
	func->locals = idht_new();
	info.locals = func->locals;
	mrnnull(func->locals);
	if (strcmp(tl->v, "void") == 0) {
		tl = tl->next;
		if (tl == NULL) {
			error("function declaration", "Unnexpected EOF.\n", tl, failp);
			return NULL;
		} else if (tl->type != TOK_NL) {
			error("function declaration", "Expected newline.\n", tl, failp);
			return NULL;
		} else {
//			goto s_fdefn;
		}
	} else {
		int i = 0;
		 while (tl->type != TOK_NL) {
			struct idhte *c = idht_index(info.types, tl->v);
			if (c == NULL) {
				error("function declaration", "Expected type name.\n", tl, failp);
				return NULL;
			}
			struct xltype *hltype = c->hltype;
			tl = tl->next;
			if (tl == NULL) {
				error("function declaration", "Expected identier, not EOF.\n", tl, failp);
				return NULL;
			} else if (tl->type != TOK_WORD) {
				error("function declaration", "Expected identifer, not non-word.\n", tl, failp);
				return NULL;
			} else if (idht_index(info.types, tl->v)) {
				error("function declaration", "Expected identifier, not type name.\n", tl, failp);
				return NULL;
			}
			struct idhte *carl = idhte_new(tl->v, hltype); // I cannot think of type names
			carl->isparam = true;
			carl->index = i;
			idht_enter(info.locals, carl);
			i++;
			func->argc = i;
			func->argv = realloc(func->argv, func->argc * sizeof(struct idhte));
			memcpy(&(func->argv[i]), carl, sizeof(struct idhte));
			tl = tl->next;
			if (tl == NULL) {
				error("function declaration", "Expected type name, not EOF.\n", tl, failp);
				return NULL;
			} else if (tl->type == TOK_NL) {
//				goto s_fdefn;
			} else if (tl->type != TOK_WORD) {
				error("function declaration", "Invalid token type instead of type name.\n", tl, failp);
				return NULL;
			}
		}
	}
//s_fdefn:
	struct block *acc = malloc(sizeof(struct block));
	func->code = acc;
	acc->next = NULL;
	acc->type = BLOCK_ERROR;
	struct idhte *type;
	int tabs = 1; // the number of tabs expected after a newline
	while (tl) {
		mrnnull(acc);
		if (tl->type == TOK_NL) {
			tl = tl->next;
			int spectabs;
			for (spectabs = 0; tl && tl->type == TOK_TAB; tl = tl->next) {
				if (verbose) {
					printf("Tab token with type %s\n", toktypenames[tl->type]);
				}
				spectabs++;
			}
			if (spectabs < tabs) {
				break;
			}
		} else if (tl->type != TOK_WORD) {
			error("function definition", "Invalid token type.\n", tl, failp);
			tl = tl->next;
//		} else if (strcmp(tl->v, "import") == 0) { // why not, import is a legal local variable
//			error("function definition", "Unnexpected import statement.\n", tl, failp);
		} else if ((type = idht_index(info.types, tl->v))) {
			acc->next = m_defn(tlp, info, failp, type->hltype);
			if (acc->next) {
				acc = acc->next;
			}
		} else {
			acc->next = malloc(sizeof(struct block));
			acc->next->type = BLOCK_EXPR;
			acc->next->exp = *m_exp(tlp, info, failp, NULL);
			acc->next->next = NULL;
		}
	}
	struct block *fout = malloc(sizeof(struct block));
	fout->type = BLOCK_FUNC;
	fout->func = *func;
	fout->next = NULL;
	return fout;
}

/**
 * @brief the parsing function for a file
 * @param tlp a pointer to the token list (the tl macro expands to a dereference of tlp
 * tlp is a pointer so that child FSMs will advance the actual input
 * since tl is a macro, tlp will get updated
 * @param info the information to be passed through each FSM
 * @param failp a pointer to a bool to show failure (used through the error function)
 * @note this only runs s_file
 */
static always_inline struct block *m_file(struct toklist **tlp, struct pinfo info, bool *failp) {
	mrnnull(tl);
	c_file(info);
	struct block *acc = malloc(sizeof(struct block));
	struct block *first = acc;
	if (tl == NULL) {
		error("file context", "Empty files are not supported.\n", tl, failp);
		return NULL;
	}
//s_file:	
	while (tl) {
		struct idhte *typ;
		mrnnull(acc);
		if (tl->type == TOK_NL) {
			tl = tl->next;
		} else if (tl->type != TOK_WORD) {
			error("file context", "Invalid token type.\n", tl, failp);
		} else if (strcmp(tl->v, "import") == 0) {
			m_imp(tlp, info, failp);
		} else if ((typ = idht_index(info.types, tl->v))) {
			acc->next = m_defn(tlp, info, failp, typ->hltype);
			if (acc->next) {
				acc = acc->next;
			}
		} else {
			error("file context", "Expected \"import\" or type.\n", tl, failp);
		}
		if (tl) {
			tl = tl->next;
		}
	}
	return first->next;
}

/**
 * @brief parse a string of tokens
 * this is where it all comes together
 * this function is called once to parse the whole file
 * @param tli the list of tokens
 * @return the AST block of the file
 */
struct block *parse(struct toklist *tli) {
	init_bp();
	struct pinfo info;
	info.globals = idht_new();
	info.types = idht_new();
	info.locals = NULL;
	info.funcs = idht_new();
	info.left = NULL; // sanity
	// bind all builtin types here
	struct idhte *foo = idhte_llnew("int", LLVMInt32Type());
	idht_enter(info.types, foo);
	foo = idhte_llnew("float", LLVMDoubleType());
	idht_enter(info.types, foo);
	foo = idhte_llnew("array", NULL);
	foo->hltype->flags = EXPECT_CONT;
	idht_enter(info.types, foo);
	foo = idhte_llnew("vec", NULL);
	foo->hltype->flags = EXPECT_CONT | EXPECT_X;
	idht_enter(info.types, foo);
	foo = idhte_llnew("mat", NULL);
	foo->hltype->flags = EXPECT_CONT | EXPECT_X | EXPECT_Y;
	idht_enter(info.types, foo);
	foo = idhte_llnew("void", LLVMVoidType());
	idht_enter(info.types, foo);
	// do the parsing
	struct toklist *tlt = tli; // temporary holder
	struct toklist **tlp = &tlt; // this will get used a lot
	bool fail = false;
	struct block *acc = m_file(tlp, info, &fail);
	if (fail) {
		fprintf(stderr, "Syntax errors were encountered. Failing compilation.\n");
		exit(128);
	} else {
		return acc;
	}
}

#ifndef DONT_TEST

#include "mtest.h"

bool sienfeld;
struct xltype jerry;

int test_parse_int(void) {
	mthead;
	struct toklist *tkl = malloc(sizeof(struct toklist));
	mrnnull(tkl);
	tkl->type = TOK_INT;
	tkl->iv = 314;
	tkl->next = NULL;
	struct expr *fred = m_ex_term(&tkl, ignore, &sienfeld, &jerry);
	mteqi(fred->type, EXPR_LIT);
	mtfalse(fred->ivc);
	mteqi(fred->lit.type, LIT_INT);
	mteqi(fred->lit.i, 314);
	mtnnull(tkl);
	free(tkl);
	free(fred);
	mttail;
}

int test_parse_float(void) {
	mthead;
	struct toklist *tkl = malloc(sizeof(struct toklist));
	mrnnull(tkl);
	tkl->type = TOK_FSTD;
	tkl->fv = 3.14;
	tkl->next = NULL;
	struct expr *fred = m_ex_term(&tkl, ignore, &sienfeld, &jerry);
	mteqi(fred->type, EXPR_LIT);
	mtfalse(fred->ivc);
	mteqi(fred->lit.type, LIT_FLOAT);
	mteqi(fred->lit.f, 3.14);
	mtnnull(tkl);
	free(tkl);
	free(fred);
	mttail;
}

int test_parse_type_pure(void) {
	mthead;
	struct toklist *tkl = lex("int i = 0"); // ignore the fact that this makes it an integration test
	struct toklist *tkli = tkl;
	mtnnull(tkl);
	struct pinfo info;
	info.types = idht_new();
	mtnnull(info.types);
	info.globals = idht_new();
	info.locals = idht_new();
	mtnnull(info.locals);
	struct idhte *foo = idhte_llnew("int", LLVMInt32Type());
	mtnnull(foo);
	idht_enter(info.types, foo);
	struct block *josh = m_defn(&tkli, info, &sienfeld, foo->hltype);
	mteqi(josh->type, BLOCK_EXPR);
	mtnull(josh->next);
	mteqi(josh->exp.type, EXPR_OP);
	mteqi(josh->exp.op.op, '=');
	mtnnull(idht_index(info.locals, "i"));
	free(josh);
	tkli = tkl;
	free(info.globals);
	info.globals = idht_new();
	free(info.locals);
	info.locals = NULL;
	josh = m_file(&tkli, info, &sienfeld);
//	mteqi(josh->type, BLOCK_EXPR);
//	mtnull(josh->next);
//	mteqi(josh->exp.type, EXPR_OP);
//	mteqi(josh->exp.op.op, '=');
	mtnull(josh);
	mtnnull(idht_index(info.globals, "i"));
	mteqs(idht_index(info.globals, "i")->hltype->t, "int");
	free(info.globals);
	free(info.types);
	free(foo);
	mttail;
}

#endif
