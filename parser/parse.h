/*
 * parse.h
 *
 * the parser
 */

#include "lex/toklist.h"
#include "ast/block.h"

/**
 * @brief parse a string of tokens
 * this is where it all comes together
 * this function is called once to parse the whole file
 * @param tl the list of tokens
 * @return the AST block of the file
 */
struct block *parse(struct toklist *tl);
